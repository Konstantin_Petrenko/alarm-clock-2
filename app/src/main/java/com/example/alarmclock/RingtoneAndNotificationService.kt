package com.example.alarmclock

import android.R
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.Ringtone
import android.media.RingtoneManager
import android.os.IBinder
import androidx.core.app.NotificationCompat


class RingtoneAndNotificationService : Service() {

    companion object {
        lateinit var ringtone: Ringtone
    }


    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        playAlarm()
        return START_NOT_STICKY
    }

    /**
     * Stop alarm
     */
    override fun onDestroy() {
        ringtone.stop()
        super.onDestroy()
    }

    /**
     * Creating and show notification
     */
    private fun myNotification() {


        val intent_main_activity =
            Intent(this.applicationContext, MainActivity::class.java)

        val pending_intent_main_activity = PendingIntent.getActivity(
            this, 0,
            intent_main_activity, 0
        )

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val channel = NotificationChannel(
            "CHANNEL_1", "My channel",
            NotificationManager.IMPORTANCE_HIGH
        )
        channel.description = "My channel description"
        notificationManager.createNotificationChannel(channel)


        val builder = NotificationCompat.Builder(this, "CHANNEL_1")
            .setSmallIcon(R.mipmap.sym_def_app_icon)
            .setContentTitle("Alarm Clock")
            .setContentText("Click for stop")
            .setContentIntent(pending_intent_main_activity)
            .setAutoCancel(true)
            .build()

        val notify_manager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notify_manager.notify(1, builder)
    }

    /**
     * Create and play alarm
     */
    private fun playAlarm() {

        val alarm = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
        ringtone = RingtoneManager.getRingtone(baseContext, alarm)
        ringtone.play()
        myNotification()
    }

}