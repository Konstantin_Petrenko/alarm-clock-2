package com.example.alarmclock

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {

        val state: String? = intent!!.getStringExtra("alarm_state")
        if (state.equals("cancel_alarm")){
            context.stopService(Intent(context, RingtoneAndNotificationService::class.java))
        } else {
            context.startService(Intent(context, RingtoneAndNotificationService::class.java))
        }

    }
}